%macro print 2
    mov edx, %1
    mov ecx, %2
    mov ebx, 1
    mov eax, 4
    int 0x80
%endmacro

section    .text
global _start

_start:
    mov ecx, 10
    mov edx, 0
    mov eax, 123
    
    div ecx
    
    mov eax, edx ; edx - reminder, eax - quotent
    add eax, '0'
    
    mov [result], eax
    print 1, result
    
    mov eax, 1
    int 0x80
    
section .data
    number dw 1
    
    
section .bss
    result resb 1