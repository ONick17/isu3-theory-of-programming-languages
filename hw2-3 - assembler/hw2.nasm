%macro print_char 1
    pushd
    jmp %%main
    %%char db %1
%%main:
    mov eax, 4
    mov ebx, 1
    mov ecx, %%char
    mov edx, 1
    int 0x80
    popd
%endmacro

%macro const_print 1
    pushd
    jmp %%print 
    %%line db %1, 0xA
    %%len equ $ - %%line
%%print:  
    mov eax, 4
    mov ebx, 1
    mov ecx, %%line
    mov edx, %%len
    int 0x80
    popd
%endmacro

%macro print 2
    pushd
    mov edx, %1
    mov ecx, %2
    mov ebx, 1
    mov eax, 4
    int 0x80
    popd
%endmacro

%macro dprint 0
    pushd
    mov bx, 0
    mov ecx, 10
    %%_divide:
        mov edx, 0
        div ecx
        push dx
        inc bx
        test eax, eax
        jnz %%_divide
    %%_digit:
        pop ax
        add ax, '0'
        mov [result], ax
        print 1, result
        dec bx
        cmp bx, 0
        jg %%_digit
    popd
%endmacro

%macro pushd 0
    push eax
    push ebx
    push ecx
    push edx
%endmacro

%macro popd 0
    pop edx
    pop ecx
    pop ebx
    pop eax
%endmacro


section .text
global _start
_start:

    pushd
    mov eax, 0
    mov bx, 0
_sum_x:
    add eax, [x+ebx]
    add bx, 4
    cmp bx, array_len
    jne _sum_x
    
    mov [x_sum], eax
    mov eax, array_len
    mov ecx, 4
    mov edx, 0
    div ecx
    mov ecx, eax
    mov eax, [x_sum]
    mov edx, 0
    div ecx
    mov [x_sum], eax
    popd
    mov eax, [x_sum]
    
    pushd
    mov eax, 0
    mov bx, 0
_sum_y:
    add eax, [y+ebx]
    add bx, 4
    cmp bx, array_len
    jne _sum_y

    mov [y_sum], eax
    mov eax, array_len
    mov ecx, 4
    mov edx, 0
    div ecx
    mov ecx, eax
    mov eax, [y_sum]
    mov edx, 0
    div ecx
    mov [y_sum], eax
    popd
    mov eax, [y_sum]
    
    mov eax, [x_sum]
    mov ecx, [y_sum]
    sub eax, ecx
    cmp eax, 0
    jge _check

    mov eax, [y_sum]
    mov ecx, [x_sum]
    sub eax, ecx
    print klen, keke
        
_check:
    dprint
    print nlen, newline
    
    print mlen, message
    print nlen, newline
    mov eax, 1
    int 0x80

section .data
    x dd 5, 3, 2, 6, 1, 7, 4
    y dd 0, 10, 1, 9, 2, 8, 5
    array_len equ $ - y
    message db "Done"
    mlen equ $ - message
    newline db 0xA, 0xD
    nlen equ $ - newline
    keke db "-"
    klen equ $ - keke

section .bss
    result resd 1
    x_sum resd 1
    y_sum resd 1