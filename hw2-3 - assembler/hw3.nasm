%macro print_char 1
    pushd
    jmp %%main
    %%char db %1
%%main:
    mov eax, 4
    mov ebx, 1
    mov ecx, %%char
    mov edx, 1
    int 0x80
    popd
%endmacro

%macro const_print 1
    pushd
    jmp %%print 
    %%line db %1, 0xA
    %%len equ $ - %%line
%%print:  
    mov eax, 4
    mov ebx, 1
    mov ecx, %%line
    mov edx, %%len
    int 0x80
    popd
%endmacro

%macro print 2
    pushd
    mov edx, %1
    mov ecx, %2
    mov ebx, 1
    mov eax, 4
    int 0x80
    popd
%endmacro

%macro dprint 0
    pushd
    mov bx, 0
    mov ecx, 10
    %%_divide:
        mov edx, 0
        div ecx
        push dx
        inc bx
        test eax, eax
        jnz %%_divide
    %%_digit:
        pop ax
        add ax, '0'
        mov [result], ax
        print 1, result
        dec bx
        cmp bx, 0
        jg %%_digit
    popd
%endmacro

%macro pushd 0
    push eax
    push ebx
    push ecx
    push edx
%endmacro

%macro popd 0
    pop edx
    pop ecx
    pop ebx
    pop eax
%endmacro


section .text
global _start
_start:
    mov eax, [num]
    mov ecx, 2
    div ecx
    mov [x1], eax
    mov eax, edx
    mov eax, [x1]
    add eax, 2
    mov ebx, 2
	mov edx, 0
    div ebx
    mov [x2], eax

_chk:
    xor eax, eax
    xor ebx, ebx
    mov eax, [x1]
    mov ebx, [x2]
    sub eax, ebx
    mov ebx, 1
    cmp eax, ebx
    jg _keke
    je _end
    jl _end

_keke:
    mov eax, [x2]
    mov [x1], eax
    pushd
    mov eax, [num]
    mov ebx, [x1]
    mov edx, 0
    div ebx
    add eax, ebx
    mov ebx, 2
    mov edx, 0
    div ebx
    mov [x2], eax
    popd
    jmp _chk

_end:
    mov eax, [x2]
    dprint
    print nlen, newline
    
    print mlen, message
    print nlen, newline
    mov eax, 1
    int 0x80

section .data
    num dd 144
    message db "Done"
    mlen equ $ - message
    newline db 0xA, 0xD
    nlen equ $ - newline

section .bss
    result resb 1
    x1 resb 10
    x2 resb 10