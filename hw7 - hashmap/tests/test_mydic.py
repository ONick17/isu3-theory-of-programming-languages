from mydic.mydic import myDic

map1 = myDic()
map1["value1"] = 1
map1["value2"] = 2
map1["value3"] = 3
map1["1"] = 10
map1["2"] = 20
map1["3"] = 30
map1["1, 5"] = 100
map1["5, 5"] = 200
map1["10, 5"] = 300

def test_iloc_1():
  assert map1.iloc(0) == 10

def test_iloc_2():
  assert map1.iloc(2) == 300

def test_iloc_3():
  assert map1.iloc(5) == 200

def test_iloc_4():
  assert map1.iloc(8) == 3


map2 = myDic()
map2["value1"] = 1
map2["value2"] = 2
map2["value3"] = 3
map2["1"] = 10
map2["2"] = 20
map2["3"] = 30
map2["(1, 5)"] = 100
map2["(5, 5)"] = 200
map2["(10, 5)"] = 300
map2["(1, 5, 3)"] = 400
map2["(5, 5, 4)"] = 500
map2["(10, 5, 5)"] = 600

def test_ploc_1():
  assert map2.ploc(">=1") == {1: 10, 2: 20, 3: 30}

def test_ploc_2():
  assert map2.ploc("<3") == {1: 10, 2: 20}

def test_ploc_3():
  assert map2.ploc(">0, >0") == {(1, 5): 100, (5, 5): 200, (10, 5): 300}

def test_ploc_4():
  assert map2.ploc(">=10, >0") == {(10, 5): 300}

def test_ploc_5():
  assert map2.ploc("<5, >=5, >=3") == {(1, 5, 3): 400}
