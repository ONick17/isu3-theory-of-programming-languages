from mydic.mydic import myDic

map1 = myDic()
map1["value1"] = 1
map1["value2"] = 2
map1["value3"] = 3
map1["1"] = 10
map1["2"] = 20
map1["3"] = 30
map1["1, 5"] = 100
map1["5, 5"] = 200
map1["10, 5"] = 300

print(str(map1))
print(map1.iloc(0))
print(map1.iloc(2))
print(map1.iloc(5))
print(map1.iloc(8))

'''
map2 = myDic()
map2["value1"] = 1
map2["value2"] = 2
map2["value3"] = 3
map2["1"] = 10
map2["2"] = 20
map2["3"] = 30
map2["(1, 5)"] = 100
map2["(5, 5)"] = 200
map2["(10, 5)"] = 300
map2["(1, 5, 3)"] = 400
map2["(5, 5, 4)"] = 500
map2["(10, 5, 5)"] = 600

print(map2)
print(map2.ploc(">=1"))
'''
