from .lexer import Lexer
from .tokens import Token, TokenType


class myParserException(Exception):
    ...


class myParser:  
    def __init__(self):
        self.tokens = []
        self.size = 0
        self.current_token = None
        self.lexer = Lexer()

    def init_parser(self, s: str):
        self.tokens = []
        self.size = 0
        self.lexer.init_lexer(s)
        self.current_token = self.lexer.next()
    
    def factor(self):
        token = self.current_token
        match token.type:
            case TokenType.SEP:

                return
            case TokenType.NUMBER:
                
                return
            case TokenType.BIGGER:
                
                return
            case TokenType.BIGGEREQ:
                
                return
            case TokenType.SMALLER:
                
                return
            case TokenType.SMALLEREQ:
                
                return
            case TokenType.EQUAL:
                
                return
            case TokenType.NOTEQUAL:
                
                return
        raise myParserException("Invalid token")

    def check_type(self, type_: TokenType):
        if self.current_token.type == type_:
            self.current_token = self.lexer.next()
            return
        #print(self.current_token)
        raise myParserException(f"Invalid token order. Expected {type_}, Received {self.current_token}")
