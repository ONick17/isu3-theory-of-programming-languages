from .tokens import TokenType, Token


class LexerException(Exception):
    ...


class Lexer:
    def __init__(self):
        self.pos = 0
        self.key = ""
        self.current_char = ""
    
    def init_lexer(self, key):
        self.pos = 0
        self.key = key
        self.current_char = self.key[self.pos]
    
    def forward(self):
        self.pos += 1
        if self.pos == len(self.key):
            self.current_char = ""
        else:
            self.current_char = self.key[self.pos]
    
    def next(self):
        while self.current_char != "":
            if self.current_char.isspace():
                self.skip()
                continue
            elif self.current_char.isdigit():
                return Token( )
            elif self.current_char == ',':
                return Token( )
            elif self.current_char == '>':
                return Token( )
            elif self.current_char == '<':
                return Token( )
            elif self.current_char == '=':
                return Token( )
            raise LexerException("Unexpected symbol")
    
    def skip(self):
        while self.current_char != "" and self.current_char.isspace():
            self.forward()

