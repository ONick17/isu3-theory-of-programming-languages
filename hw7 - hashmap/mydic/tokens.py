from enum import Enum, auto


class TokenType(Enum):
    SEP = ','
    NUMBER = auto()
    BIGGER = '>'
    BIGGEREQ = ">="
    SMALLER = "<"
    SMALLEREQ = "<="
    EQUAL = '='
    NOTEQUAL = "<>"


class Token:
    def __init__(self, type_, val):
        self.type = type_
        self.val = val
    
    def __str__(self):
        return f"Token ({self.type}, {self.value})"
