from .myparser import myParser
from .tokens import TokenType


class myDicException(Exception):
    ...


class myDic():
    def __init__(self, dic: dict = {}):
        self.dic = dic

    def __getitem__(self, key):
        if isinstance(key, str):
            key = key.replace('(', '') 
            key = key.replace(')', '')
        return self.dic[key]
    
    def __setitem__(self, key, value):
        if type(key) == str:
            key = key.replace('(', '') 
            key = key.replace(')', '')
        self.dic[key] = value
        self.dic = self.toSortedSet()
    
    def toSortedSet(self):
        keke = {}
        keys = sorted(self.dic.keys())
        for i in keys:
            keke[i] = self.dic[i]
        return keke

    def __str__(self):
        return(str(self.dic))

    def iloc(self, ind):
        if ind in range(0, len(self.dic)):
            return list(self.dic.values())[ind]
        else:
            raise myDicException("Index out of range")
    
    def ploc(self, ind):
        pass