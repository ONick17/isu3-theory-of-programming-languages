import json

class PointError(Exception):
    pass

class Point:
    def __init__(self, x: float, y: float, code: str = "no_code") -> None:
        if not (isinstance(x, int) or isinstance(x, float)) or not (isinstance(y, int) or isinstance(y, float)):
            raise PointError("x, y should be integer type")
        if not isinstance(code, str):
            raise PointError("code should be string type")
        self.code=code
        self.x=float(x)
        self.y=float(y)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Point):
            if hasattr(other, "__iter__"):
                return self == Point(*other)
            else:
                raise NotImplementedError
        return self.x == other.x and self.y == other.y

    def __neg__(self) -> "Point":
        return Point(-self.x, -self.y)

    def distance_to(self, other: "Point") -> float:
        return ((abs(self.x-other.x))**2 + (abs(self.y-other.y))**2) ** 0.5

    def triangle_square(self, other1: "Point", other2: "Point") -> float:
        a = self.distance_to(other1)
        b = self.distance_to(other2)
        c = other1.distance_to(other2)
        p = (a+b+c)/2.0
        return (p * (p - a) * (p - b) * (p - c))**0.5
    
    def to_json(self) -> str:
        return json.dumps({"x": self.x, "y": self.y, "code": self.code})

    @classmethod
    def from_json(cls: type, s: str) -> "Point":
        js = json.loads(s)
        if "code" in js:
            code = js["code"]
        else:
            code = "no_code"
        return cls(int(js["x"]), int(js["y"]), code)

    def __str__(self) -> str:
        return f"{self.__class__.__name__}({self.x}, {self.y}, {self.code})"

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.x}, {self.y}, {self.code})"

    def is_center(self) -> bool:
        return self == Point(0, 0)

class PointsError(Exception):
    pass

class Points:
    def __read_dic(self, lst):
        cnt = 0
        for i in lst:
            if "location" in i:
                if ("lat" in i["location"]) and ("lon" in i["location"]):       
                    if "code" in i:
                        code = i["code"]
                    else:
                        code = str(cnt)        
                    self.data.append(Point(i['location']['lat'], i['location']['lon'], code))
                    cnt += 1
    
    def __init__(self, other = None) -> None:
        self.data = []
        if other:
            if type(other) == str:
                self.__read_dic(json.load(open(other)))
            elif type(other) == list:
                if type(other[0]) == dict:
                    self.__read_dic(other)
                elif type(other[0]) == Point:
                    self.data = other.copy()
                

    def append(self, other: Point) -> None:
        self.data.append(other)

    def find_smallest(self) -> list:
        ans = [self.data[0], self.data[1], self.data[2]]
        mn = self.data[0].triangle_square(self.data[1], self.data[2])
        for i1 in range(len(self.data)-2):
            for i2 in range(i1+1, len(self.data)-1):
                for i3 in range(i2+1, len(self.data)):
                    square = self.data[i1].triangle_square(self.data[i2], self.data[i3])
                    if (mn > square):
                        mn = square
                        ans = [self.data[i1], self.data[i2], self.data[i3]]
        return ans