import pytest
import json
from plib import Point, PointError, Points, PointsError


@pytest.fixture
def points():
    return Point(0, 0), Point(0, 5)


class TestPoint:
    def test_creation1(self):
        assert Point(1, 2)

    def test_creation2(self):
        assert Point(1, 2, "1")

    def test_distance_to(self, points):
        p1, p2 = points
        assert p1.distance_to(p2) == 5.0

    @pytest.mark.parametrize(
        "p1, p2, distance",
        [(Point(0, 0), Point(0, 10), 10),
         (Point(0, 0), Point(10, 0), 10),
         (Point(0, 0), Point(0, 1), 1)
        ]
    )

    def test_distance_all_axis(self, p1, p2, distance):
        assert p1.distance_to(p2) == pytest.approx(distance, 0.1)

    def test_to_json(self):
        js = '{"x": 0.0, "y": 0.0, "code": "no_code"}'
        assert Point(0, 0).to_json() == js

    def test_from_json(self):
        js = '{"x":0, "y":0}'
        assert Point.from_json(js) == Point(0, 0)

    def test_center(self, points):
        p1, p2 = points
        assert p1.is_center()
        assert not p2.is_center()

    def test_eq_with_other_type(self, points):
        p1, p2 = points
        assert p1 == [0, 0]
        assert p2 == (0, 5)



def points2():
    return [{"location": {"lat": 0, "lon": 0}}, 
            {"location": {"lat": 0, "lon": 1}, "code": "keke1"}, 
            {"location": {"lat": 1}, "code": "keke2"}, 
            {"location": {"lat": 1, "lon": 1}, "code": "keke3"}, 
            {"code": "keke4"}, 
            {"location": {"lat": 0, "lon": 7}}]

def points3():
    return [Point(0, 0), Point(0, 1), Point(1, 2), Point(1, 0)]

class TestPoints:
    def test_creation1(self):
        assert Points()
        
    def test_creation2(self):
        assert Points(points2())
        
    def test_creation3(self):
        assert Points("stations.json")
        
    def test_creation4(self):
        assert Points(points3())
        
    def test_append(self):
        a = Points()
        a.append(Point(0, 0))
        assert a.data == Points([Point(0, 0)]).data
        
    def test_find_smallest(self):
        assert Points(points3()).find_smallest() == [Point(0, 0), Point(0, 1), Point(1, 2)]