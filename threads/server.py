import socket
from datetime import datetime
import time
from threading import Thread

def process_client(conn, addr):
    with conn:
        data = b''
        while True:
            try:
                data = conn.recv(1024)
                if data == b"get":
                    now = datetime.utcnow()
                    conn.sendall(str(now).encode())
            except BlockingIOError:
                time.sleep(0.02)
                continue
            if not data:
                break
    print(f"Client socket closed {addr}")

host = "127.0.0.1"
port = 2001

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((host, port))
    sock.setblocking(False)
    sock.listen(3)
    threads = []
    while True:
        # print("Wait for new client")
        try:
            conn, addr = sock.accept()
            print(f"New client accepted {addr}")
            t = Thread(target=process_client, args=(conn, addr))
            t.start()
            threads.append(t)
            for th in threads[:]:
                if not th.is_alive():
                    threads.remove(th)
            print(f"Client threads = {len(threads)}")
        except BlockingIOError:
            pass
            #time.sleep(0.01)
