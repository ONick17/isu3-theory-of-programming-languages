import socket
import time

host = "127.0.0.1"
port = 2001

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((host,port))
    while True:
        sock.send(b"get")
        print(sock.recv(1024))
        time.sleep(1)

