fl = input()
fl = open(fl)
fl = fl.read().replace('\n', ' ')
fl = fl.split()

coms = ["MoO", "MOo", "moO", "mOo", "moo", "MOO", "OOM", "oom", "Moo", "OOO"]
i = 0
while i < len(fl):
    if fl[i] not in coms:
        fl.pop(i)
    else:
        i += 1

cycles = []
fl2 = fl.copy()
while ("MOO" in fl2) and ("moo" in fl2):
    i = 0
    cycle_start = 0
    while True:
        if fl2[i] == "MOO":
            cycle_start = i
        elif fl2[i] == "moo":
            cycles.append(cycle_start)
            cycles.append(i)
            fl2[cycle_start] = "start"
            fl2[i] = "end"
            break
        i += 1

'''
cnt = 1
for i in fl:
    if i == "moo":
        print(cnt, ":", i, "START")
        cnt += 1
    elif i == "MOO":
        print(cnt, ":", i, "END")
        cnt += 1
    else:
        print(cnt, ":", i)
        cnt += 1
print("!!!!!!")

funcs = []
func=[]
i = 0
while i < len(fl):
    if fl[i] == 'moo':
        i += 1
        while True:
            if i < len(fl):
                if fl[i] == "MOO":
                    funcs.append(func)
                    func = []
                    break
                else:
                    func.append(fl[i])
                i += 1
            else:
                funcs.append(func)
                break
    else:
        i += 1

cnt = 1
for i in fl:
    print(cnt, ":", i)
    cnt += 1
'''

stack = [0]*1000

def do_cow(fl):
    global stack
    global cycles
    i = 0
    j = 0
    while i < len(fl):
        if fl[i] == "MoO": #1 значение текущей ячейки увеличить на 1
            stack[j] += 1
        elif fl[i] == "MOo": #2 значение ​текущей ячейки уменьшить на 1
            stack[j] -= 1
        elif fl[i] == 'moO': #3 следующая ячейка
            j += 1
        elif fl[i] == 'mOo': #4 предыдущая ячейка
            j -= 1
        elif fl[i] == 'moo': #5 начало цикла
            i = cycles[cycles.index(i)-1]
            continue
        elif fl[i] == 'MOO': #6 конец цикла
            if stack[j] == 0:
                i = cycles[cycles.index(i)+1]
        elif fl[i] == 'OOM': #7 вывод значения текущей ячейки
            print(stack[j], end='')
        elif fl[i] == 'oom': #8 ввод значения в текущую ячейку
            stack[j] = int(input("Введите свое значение: "))
        elif fl[i] == 'Moo': #10 если значение в ячейке равно 0,
                            #то ввести с клавиатуры, если значение не 0,
                            #то вывести на экран
            if stack[j] != 0:
                print(chr(stack[j]), end='')
            else:
                stack[j] = int(input("Введите свое значение: "))
        elif fl[i] == 'OOO': #11 обнулить значение в ячейке
            stack[j] = 0
        i += 1

do_cow(fl)
