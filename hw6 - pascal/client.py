import zmq
import json


context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('tcp://127.0.0.1:43000')

while True:
    mode = None
    while mode not in ['t', 'v', 'b', 'e']:
        mode = input("Tree(t), variables(v), both(b) or exit(e): ")
    if mode == 'e':
        break
    fl = input("Path to file with code: ")
    with open(fl, 'r') as fl:
        code = fl.read()
    #print(code)
    
    socket.send(code.encode())
    keke = json.loads(socket.recv().decode())
    
    if mode == 't':
        print(keke["tree"])
    elif mode == 'v':
        print(keke["vrs"])
    elif mode == 'b':
        print(keke["tree"])
        print(keke["vrs"])
