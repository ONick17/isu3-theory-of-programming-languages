from ilib.interpreter import Interpreter
#from ilib.tokens import Token, TokenType

with open("./tests/test2.txt", 'r') as f:
    code = f.read()
#print(code)

interp = Interpreter()
tree, vrs = interp.eval(code)
print(tree)
print(vrs)
