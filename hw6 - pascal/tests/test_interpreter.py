from ilib.interpreter import Interpreter
from ilib.myparser import myParser
from ilib.lexer import Lexer
from ilib.tokens import TokenType

def test_interpreter_1():
  interp = Interpreter()
  #with open('C:\Documents\IT\3c-tpl\hw6 - pascal\tests\test1.txt', 'r') as f:
  a = \
"""BEGIN
END."""
  _, a = interp.eval(a)
  assert a == {}

def test_interpreter_2():
  interp = Interpreter()
  #with open('C:\Documents\IT\3c-tpl\hw6 - pascal\tests\test2.txt', 'r') as f:
  a = \
"""BEGIN
	x:= 2 + 3 * (2 + 3);
  y:= 2 / 2 - 2 + 3 * ((1 + 1) + (1 + 1));
END."""
  _, a = interp.eval(a)
  assert a == {'x': 17.0, 'y': 11.0}

def test_interpreter_3():
  interp = Interpreter()
  #with open('C:\Documents\IT\3c-tpl\hw6 - pascal\tests\test3.txt', 'r') as f:
  a = \
"""BEGIN
  y: = 2;
  BEGIN
    a := 3;
    a := a;
    b := 10 + a + 10 * y / 4;
    c := a - b
  END;
  x := 11;
END."""
  _, a = interp.eval(a)
  assert a == {'y': 2.0, 'a': 3.0, 'b': 18.0, 'c': -15.0, 'x': 11.0}

def test_lexer_1():
  lexer = Lexer()
  assert True == True

def test_lexer_1():
  lexer = Lexer()
  lexer.init_lexer(
"""BeGiN
beginning : = 17;
eNd.""")
  lst = []
  keke = lexer.next()
  while keke.type != TokenType.EOL:
      lst.append(keke.type.name)
      keke = lexer.next()
  assert lst == ['BEGIN', 'ID', 'ASSIGN', 'NUMBER', 'SEMI', 'END', 'DOT']

'''
def test_lexer_2():
  lexer = Lexer()
  lexer.init_lexer(
"""BEGIN
x := 17;
END.""")
  lst = []
  keke = lexer.next()
  while keke.type != TokenType.EOL:
      lst.append(keke.type.value)
      keke = lexer.next()
  assert lst == ['BEGIN', 12, 11, 1, 10, 'END', 9]
'''
def test_lexer_2():
  lexer = Lexer()
  lexer.init_lexer(
"""BEGIN
x := 17;
END.""")
  lst = []
  keke = lexer.next()
  while keke.type != TokenType.EOL:
      lst.append(keke.type.value)
      keke = lexer.next()
  assert lst == ['BEGIN', 'VARIABLE', ':=', 'NUMBER', ';', 'END', '.']

def test_parser():
  parser = myParser()
  keke = parser.init_parser("""BEGIN
beginning := 17;
END.""")
  assert str(keke) == """Code 0:
Line 0, variable beginning:
Number(TokenType.NUMBER, 17)"""
