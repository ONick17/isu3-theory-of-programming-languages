from .myparser import myParser
from .tree import NodeVisitor, Node, BinOp, Number, UnaryOp, Variable
from .tokens import TokenType

class InterpreterException(Exception):
    ...

class Interpreter(NodeVisitor):
    def __init__(self):
        self.parser = myParser()
        self.tree = None
        self.vars = None

    def eval(self, s: str):
        self.tree = self.parser.init_parser(s)
        self.vars = {}
        self.visit_code(self.tree)
        return self.tree, self.vars

    def visit(self, node: Node) -> float:
        match node:
            case Variable():
                return self.visit_variable(node)
            case BinOp():
                return self.visit_binop(node)
            case UnaryOp():
                return self.visit_unary(node)
            case Number():
                return self.visit_number(node)
        raise InterpreterException("invalid node")

    def visit_code(self, node):
        lines = node.lines.copy()
        for i in node.codes:
            lines.extend(self.visit_subcode(i))
        lines = sorted(lines, key=lambda i: i.id)
        for i in lines:
            #print(i)
            self.visit_line(i)

    def visit_subcode(self, node, lines=[]):
        lines.extend(node.lines)
        for i in node.codes:
            lines.extend(self.visit_subcode(i))
        return lines

    def visit_line(self, node):
        #print(node.var.value)
        self.vars[node.var.value] = self.visit(node.expr)

    def visit_unary(self, node: Number) -> float:
        match node.op.type:
            case TokenType.PLUS:
                return + self.visit(node.node)
            case TokenType.MINUS:
                return - self.visit(node.node)
        raise InterpreterException("invalid unary operator")

    def visit_binop(self, node: BinOp) -> float:
        match node.op.type:
            case TokenType.PLUS:
                return self.visit(node.left) + self.visit(node.right)
            case TokenType.MINUS:
                return self.visit(node.left) - self.visit(node.right)
            case TokenType.MUL:
                return self.visit(node.left) * self.visit(node.right)
            case TokenType.DIV:
                return self.visit(node.left) / self.visit(node.right)
        raise InterpreterException("invalid binop operator")

    def visit_number(self, node: Number) -> float:
        # print(f"Visit number {node}")
        return float(node.value.value)

    def visit_variable(self, node):
        if node.value.value in self.vars.keys():
            return float(self.vars[node.value.value])
        raise InterpreterException("There is no "+ str(node.value.value))
