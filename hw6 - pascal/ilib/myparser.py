from .tokens import TokenType
from .tree import Node, BinOp, Number, UnaryOp, Line, Code, Variable
from .lexer import Lexer

class ParserException(Exception):
    ...

class myParser:
    def __init__(self):
        self.current_token = None
        self.lexer = Lexer()
        self.tree = None
        self.cnt_lines = None
        self.cnt_codes = None

    def check_type(self, type_: TokenType):
        if self.current_token.type == type_:
            self.current_token = self.lexer.next()
            return
        #print(self.current_token)
        raise ParserException(f"Invalid token order. Expected {type_}, Received {self.current_token}")

    def factor(self) -> Node:
        token = self.current_token
        match token.type:            
            case TokenType.PLUS:
                self.check_type(TokenType.PLUS)
                return UnaryOp(token, self.factor())
            case TokenType.MINUS:
                self.check_type(TokenType.MINUS)
                return UnaryOp(token, self.factor())
            case TokenType.NUMBER:
                self.check_type(TokenType.NUMBER)
                return Number(token)
            case TokenType.LPAREN:
                self.check_type(TokenType.LPAREN)
                result = self.expr()
                self.check_type(TokenType.RPAREN)
                return result
            case TokenType.ID:
                self.check_type(TokenType.ID)
                return Variable(token)
        raise ParserException("Invalid factor", token)

    def term(self) -> Node:
        ops = [TokenType.DIV, TokenType.MUL]
        result = self.factor()
        while self.current_token.type in ops:
            token = self.current_token
            match token.type:
                case TokenType.DIV:
                    self.check_type(TokenType.DIV)
                    # result /= self.factor()
                case TokenType.MUL:
                    self.check_type(TokenType.MUL)
                    # result *= self.factor()
            result = BinOp(result, token, self.factor())
        return result

    def expr(self) -> Node:
        ops = [TokenType.PLUS, TokenType.MINUS]
        result = self.term()
        while self.current_token.type in ops:
            token = self.current_token
            match token.type:
                case TokenType.PLUS:
                    self.check_type(TokenType.PLUS)
                    # result += self.term()
                case TokenType.MINUS:
                    self.check_type(TokenType.MINUS)
                    # result -= self.term()
            result = BinOp(result, token, self.term())
        return result

    def init_parser(self, s: str):
        self.lexer.init_lexer(s)
        self.current_token = self.lexer.next()
        self.check_type(TokenType.BEGIN)
        self.cnt_lines = -1
        self.cnt_codes = -1
        self.tree = self.code()
        #print(self.current_token)
        return self.tree

    def line(self):
        if (self.current_token is None):
            raise ParserException("Current token is not defined")
        var = self.current_token
        self.check_type(TokenType.ID)
        self.check_type(TokenType.ASSIGN)
        expr = self.expr()
        #print(expr)
        if self.current_token.type == TokenType.SEMI:
            self.check_type(TokenType.SEMI)
        elif self.current_token.type != TokenType.END:
            raise ParserException("Expected end or semi")
        self.cnt_lines += 1
        return Line(self.cnt_lines, var, expr)

    def code(self):
        self.cnt_codes += 1
        if (self.current_token is None):
            raise ParserException("Current token is not defined")
        code = Code(self.cnt_codes)
        while (self.current_token.type != TokenType.END):
            #print(self.current_token)
            if self.current_token.type == TokenType.BEGIN:
                self.check_type(TokenType.BEGIN)
                code.addCode(self.code())
            else:
                code.addLine(self.line())
        self.check_type(TokenType.END)
        if self.current_token.type == TokenType.SEMI:
            self.check_type(TokenType.SEMI)
        elif self.current_token.type == TokenType.DOT:
            self.check_type(TokenType.DOT)
            if self.current_token.type != TokenType.EOL:
                raise ParserException("Dot must be at the very end.")
        return code
