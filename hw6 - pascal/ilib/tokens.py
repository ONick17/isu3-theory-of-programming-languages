from enum import Enum, auto

'''
class TokenType(Enum):
    NUMBER = auto() #1
    PLUS = auto() #2
    MINUS = auto() #3
    DIV = auto() #4
    MUL = auto() #5
    EOL = auto() #6
    LPAREN = auto() #7
    RPAREN = auto() #8
    BEGIN = "BEGIN" 
    END = "END"
    DOT = auto() #9
    SEMI = auto() #10
    ASSIGN = auto() #11
    ID = auto() #12
'''

class TokenType(Enum):
    NUMBER = "NUMBER"
    PLUS = "+"
    MINUS = "-"
    DIV = "/"
    MUL = "*"
    EOL = "EOL"
    LPAREN = "("
    RPAREN = ")"
    BEGIN = "BEGIN" 
    END = "END"
    DOT = "."
    SEMI = ";"
    ASSIGN = ":="
    ID = "VARIABLE"


class Token:
    def __init__(self, type_: TokenType, value: str):
        self.type = type_
        self.value = value

    def __str__(self) -> str:
        return f"{self.type}, {self.value}"
