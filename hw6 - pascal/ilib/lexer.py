from .tokens import Token, TokenType


class LexerException(Exception):
    ...

class Lexer:
    def __init__(self):
        self.pos = 0
        self.text = ""
        self.current_char = ""

    def init_lexer(self, s: str):
        self.pos = 0
        self.text = s
        self.current_char = self.text[self.pos]

    def forward(self):
        self.pos += 1
        if self.pos == len(self.text):
            self.current_char = ""
        else:
            self.current_char = self.text[self.pos]

    def next(self) -> Token:
        signs = ["+", "-", "*", "/", " ", ".", "(", ")", "\n", ":", ";", "="]
        while self.current_char != "":
            #print(self.current_char)
            if self.current_char.isdigit():
                return Token(TokenType.NUMBER, self.number())
            elif self.current_char == "+":
                ch = self.current_char
                self.forward()
                return Token(TokenType.PLUS, ch)
            elif self.current_char == "-":
                ch = self.current_char
                self.forward()
                return Token(TokenType.MINUS, ch)
            elif self.current_char == "*":
                ch = self.current_char
                self.forward()
                return Token(TokenType.MUL, ch)
            elif self.current_char == "/":
                ch = self.current_char
                self.forward()
                return Token(TokenType.DIV, ch)
            elif self.current_char == "(":
                ch = self.current_char
                self.forward()
                return Token(TokenType.LPAREN, ch)
            elif self.current_char == ")":
                ch = self.current_char
                self.forward()
                return Token(TokenType.RPAREN, ch)
            elif self.current_char.isspace():
                self.skip()
                continue
            elif self.current_char.isalpha() or self.current_char == "_":
                if (self.text[self.pos:self.pos+len(TokenType.BEGIN.value)].upper() == TokenType.BEGIN.value) \
                    and (self.text[self.pos+len(TokenType.BEGIN.value)].isspace()):
                        for i in range(len(TokenType.BEGIN.value)):
                            self.forward()
                        return Token(TokenType.BEGIN, TokenType.BEGIN.value)
                elif (self.text[self.pos:self.pos+len(TokenType.END.value)].upper() == TokenType.END.value) \
                    and ((self.text[self.pos+len(TokenType.END.value)].isspace())
                    or (self.text[self.pos+len(TokenType.END.value)] == ".")
                    or (self.text[self.pos+len(TokenType.END.value)] == ";")):
                        for i in range(len(TokenType.END.value)):
                            self.forward()
                        return Token(TokenType.END, TokenType.END.value)
                else:
                    ans = self.current_char
                    self.forward()
                    while self.current_char not in signs:
                        ans += self.current_char
                        self.forward()
                    return Token(TokenType.ID, ans)
            elif self.current_char == '.':
                ch = self.current_char
                self.forward()
                return Token(TokenType.DOT, ch)
            elif self.current_char == ';':
                ch = self.current_char
                self.forward()
                return Token(TokenType.SEMI, ch)
            elif self.current_char == ':':
                self.forward()
                if self.current_char.isspace():
                    self.skip()
                if self.current_char == '=':
                    self.forward()
                    return Token(TokenType.ASSIGN, ":=")
                else:
                    raise LexerException("Expected equal sign")
            #print(self.current_char)
            raise LexerException("Bad token" + self.current_char)
        #print("check")
        return Token(TokenType.EOL, "")

    def skip(self):
        while self.current_char != "" and self.current_char.isspace():
            self.forward()
    
    def number(self) -> str:
        result = []
        while self.current_char != "" and \
                (self.current_char.isdigit() or 
                self.current_char == '.'):
            result.append(self.current_char)
            self.forward()
        return "".join(result)
