from .tokens import Token, TokenType
from abc import ABC

class Node(ABC):
    ...

class Number(Node):
    def __init__(self, value: Token):
        self.value = value

    def __str__(self):
        return f"{self.__class__.__name__}({self.value})"

class BinOp(Node):
    def __init__(self, left: Node, op: Token, right: Node):
        self.left = left
        self.op = op
        self.right = right
    def __str__(self):
        return f"BinOp{self.op.value} ({self.left}, {self.right})"

class UnaryOp(Node):
    def __init__(self, op: Token, node: Node):
        self.op = op
        self.node = node
    def __str__(self):
        return f"UnaryOp{self.op.value} ({self.node})"

class NodeVisitor:
    def visit(self, node: Node) -> float:
        raise NotImplementedError

class Variable(Node):
  def __init__(self, value: Token) -> None:
    self.value = value

  def __str__(self) -> str:
    return f"Variable ({self.value})"

class Line(Node):
  def __init__(self, id: int, var: Token, expr: Node) -> None:
    self.id = id
    self.var = var
    self.expr = expr

  def __str__(self) -> str:
    return f"Line {self.id}, variable {self.var.value}:\n{self.expr}"    

class Code(Node):
  def __init__(self, id_: int) -> None:
    self.id = id_
    self.lines = []
    self.codes = []

  def addCode(self, code):
    self.codes.append(code)

  def addLine(self, line):
    self.lines.append(line)

  def __str__(self):
    ans = "Code " + str(self.id) + ":\n"
    for i in self.lines:
      ans += str(i) + "\n"
    ans = ans[:-1]
    for i in self.codes:
      ans += "\n\n" + str(i)
    return ans