import zmq
import json
from ilib.interpreter import Interpreter


context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind('tcp://127.0.0.1:43000')

inter = Interpreter()

while True:
    code = socket.recv().decode()
    #print(code)
    tree, vrs = inter.eval(code)
    keke = {"tree": str(tree), "vrs": vrs}
    keke = json.dumps(keke)
    socket.send(keke.encode())
